# Keycloak Spring Boot resource server

This is a simple resource server for Keycloak. It is configured to work on a local instance of Keycloak. This is one part of a three part system involving a _Authorization Server_ (Keycloak), a _Resource Server_ (Java Spring Application), and a _Client_ (simple JS applicatio).

The resource server is configured to accept JWTs and validate them again our locally running instance of keycloak (on port 8083).

## Setup

Have a local instance of Keycloak running on port 8083. In Keycloak, configure:

- A realm
- A client to run locally, this client logs in and gets access tokens.
- A user with either Administrator or User roles.
- The client has the Role scope included.

## Install

Clone the repository, run Keycloak, run the client application, then add a database to PostgreSQL. Configure the application.properties file to point to that database instance. 

Ensure issuer-uri and jwk-set-uri are configured correctly.

## Usage

Run the application through Intellij, get an access token from the client. Then attach the token as a Bearer authorization header to the endpoints. 

## Maintainers
[Nicholas Lennox (@NicholasLennox)](https://gitlab.com/NicholasLennox)

## Licence

Copyright 2021, Noroff Accelerate AS
