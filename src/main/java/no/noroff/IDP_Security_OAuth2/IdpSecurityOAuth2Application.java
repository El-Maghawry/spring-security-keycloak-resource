package no.noroff.IDP_Security_OAuth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdpSecurityOAuth2Application {

	public static void main(String[] args) {
		SpringApplication.run(IdpSecurityOAuth2Application.class, args);
	}

}
