package no.noroff.IDP_Security_OAuth2.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 This class is to show role based authorization.
 These roles come from Keycloak and are in the JWT and on our db
*/

@RestController
@RequestMapping("/api/resources")
public class ResourceController {

    @GetMapping("/user")
    public String getUserResources(){
        return "User resources";
    }


    @GetMapping("/admin")
    public String getAdminResources(){
        return "Admin resources";
    }
}
