package no.noroff.IDP_Security_OAuth2.repositories;

import no.noroff.IDP_Security_OAuth2.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role getRoleByName(String name);
}
