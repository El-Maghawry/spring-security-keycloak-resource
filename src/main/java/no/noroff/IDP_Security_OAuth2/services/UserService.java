package no.noroff.IDP_Security_OAuth2.services;

import no.noroff.IDP_Security_OAuth2.models.Role;
import no.noroff.IDP_Security_OAuth2.models.RoleType;
import no.noroff.IDP_Security_OAuth2.models.User;
import no.noroff.IDP_Security_OAuth2.repositories.RoleRepository;
import no.noroff.IDP_Security_OAuth2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/*
 This service exists to manage our database user profiles,
 it essentially is helping tie what we have on the resource server to our Keycloak auth server
*/
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    public boolean checkIfUserExists(String email){
        return userRepository.existsByEmail(email);
    }

    public boolean assignRolesToUser(User user, List<RoleType> roles){
        return false;
    }

    public User createNewUserProfileFromJWT(Jwt principal) {
        User newUser = new User();
        newUser.setEmail(principal.getClaimAsString("email"));
        newUser.setName(principal.getClaimAsString("given_name"));
        newUser.setSurname(principal.getClaimAsString("family_name"));
        // There are cleaner ways of doing this - this is just an example
        List<String> roles = principal.getClaimAsStringList("roles");
        HashSet<Role> dbRoles = new HashSet<>();
        for (String role: roles) {
            dbRoles.add(roleRepository.getRoleByName(role));
        }
        newUser.setRoles(dbRoles);
        newUser = userRepository.save(newUser);

        return newUser;
    }
}
